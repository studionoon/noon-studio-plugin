// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sass = require('gulp-sass')(require('sass'));

const files = { 

    scssPath: [ 
        'admin/sass/*.scss',
        'admin/sass/*/*.scss',
    ],
    
}

function scssTask(){     
    return src( files.scssPath ) 
        .pipe(sass()) // compile SCSS to CSS
        .pipe(dest('admin/css'))
}

// Watch task: watch SCSS and JS files for changes
function watchTask(){
    watch(files.scssPath,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( scssTask )
        )
    );    
}

// Export 
exports.default = series(
    scssTask,
    watchTask
);