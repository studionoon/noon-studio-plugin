<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://noon.studio
 * @since      1.0.0
 *
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/includes
 * @author     Studio Noon <matt@noon.studio>
 */
class Noon_Studio_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
