<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://noon.studio
 * @since      1.0.0
 *
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/admin
 * @author     Studio Noon <matt@noon.studio>
 */
class Noon_Studio_Plugin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Noon_Studio_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Noon_Studio_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/noon-studio-plugin-admin.css', array(), time(), 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Noon_Studio_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Noon_Studio_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/noon-studio-plugin-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', array( 'jquery' ), $this->version, false );

	}

	public function acf_json_save_point( $path ){
		
		return dirname(__FILE__) . '/acf_json';
		
	}

    public function acf_json_load_point( $path ){
		
		$paths[] = dirname(__FILE__) . '/acf_json';
		return $paths;		
		
	}

	// Create ACF Admin
	public function add_acf_options() {

		// Check function exists.
		if( function_exists('acf_add_options_page') ) {

			// Register options page.
			$option_page = acf_add_options_page(array(
				'page_title'    => __('Studio Noon Settings'),
				'menu_title'    => __('Noon Settings'),
				'menu_slug'     => 'studio-noon-settings',
				'capability'    => 'edit_posts',
				'redirect'      => false,
				'show_in_graphql' => true
			));

			// Add contact.
			$contact = acf_add_options_page(array(
				'page_title'  => __('Contact Settings'),
				'menu_title'  => __('Contact'),
				'parent_slug' => $option_page['menu_slug'],
				'show_in_graphql' => true
			));

		}

	}


	// Register Menus

    public function wpb_custom_footer_menus() {
        register_nav_menus(
            array(

				//	Primary Live
				'primary' => __( 'Primary' ),
				//	Primary Debug
				'primary-debug' => __( 'Debug' ),
                //	Footer - 1
                'footer-1' => __( 'Footer - 1' ),
                //	Footer - 2
                'footer-2' => __( 'Footer - 2' ),
            )
        );
    }


    // public function remove_block_css() {
    //     wp_dequeue_style( 'wp-block-columns' ); // WordPress core
    //     wp_dequeue_style( 'wp-block-column' ); // WordPress core
    //     }

    public function custom_render_block_core_group (
        string $block_content, 
        array $block
    ): string 
    {
        if (
            $block['blockName'] === 'core/group' &&  !is_admin() && !wp_is_json_request() ) {
            $html = '';
    
            $block['attrs']['className'] = '';

            // $block['attrs']['className'] = str_replace('wp-block-group', 'group', $block['attrs']['className']);



            // rows
            $block['attrs']['className'] = str_replace('wp-block-column', 'container', $block['attrs']['className']);
            $block['attrs']['className'] = str_replace('wp-block-columns', 'row', $block['attrs']['className']);
    
    
            // $html .= '<div class="group ' . $block['attrs']['className'] . '">' . "\n";
            // $html .= '<div class="container">' . "\n";
    
            // if (isset($block['innerBlocks'])) {
            //     foreach ($block['innerBlocks'] as $inner_block) {
            //         $html .= render_block($inner_block);
            //     }
            // }
    
            $html .= '</div><!--/ .container -->' . "\n";
            $html .= '</div><!--/ .group -->' . "\n";
    
            return $html;
        }
    
        return $block_content;
    }

    public function bootstrap_column_str_replace($block_content, $block) {
        // Only affect Core Heading and Core Paragraph blocks
        if( 'core/columns' === $block['blockName'] ) {
            $block_content = str_replace('wp-block-columns', 'row', $block_content);
            $block_content = str_replace('wp-block-column', 'col', $block_content);
            // $block_content = str_replace('text-align:center', '', $block_content);
        }

        if( 'core/group' === $block['blockName'] ) {
            $block_content = str_replace('wp-block-group__inner-container', 'container', $block_content);
            $block_content = str_replace('wp-block-group', 'wrapper', $block_content);

            // $block_content = str_replace('text-align:center', '', $block_content);
        }

        // Always return the content
        return $block_content;
    }
    

	public function yoast_images_sizes(){
        
        add_filter( 'big_image_size_threshold', '__return_false' );

        add_image_size( 'og_image', 1200, 627, true );
        add_image_size( 'logo_image', 150, 150, false );
        
    }

	public function set_yoast_image_size(){

        return 'og_image';

    }

	public function add_featured_images(){

        add_theme_support( 'post-thumbnails' );

    }

    public function add_images_sizes_to_images( $sizes ){

        return array_merge( $sizes, array(
            'og_image' => __( 'Open Graph' ),
            'logo_image' => __( 'Logo Image' )
        ) );

    }
    

	public function editor_colours(){

		add_theme_support( 'editor-color-palette', array(
			array(
				'name'  => __( 'Primary 100', $this->plugin_name  ),
				'slug'  => 'primary-100',
				'color'	=> '#02182B',
			),
			array(
				'name'  => __( 'Primary 95', $this->plugin_name  ),
				'slug'  => 'primary-95',
				'color' => '#0e2335',
			),
			array(
				'name'  => __( 'Primary 65', $this->plugin_name  ),
				'slug'  => 'primary-65',
				'color' => '#5D6874',
			),
			array(
				'name'	=> __( 'Primary 5', $this->plugin_name  ),
				'slug'	=> 'primary-5',
				'color'	=> '#eeeff1',
			),
			
			array(
				'name'	=> __( 'Accent 100', $this->plugin_name  ),
				'slug'	=> 'accent-100',
				'color'	=> '#ED5C5A',
			),
			array(
				'name'	=> __( 'Accent 80', $this->plugin_name  ),
				'slug'	=> 'accent-80',
				'color'	=> '#F17C7A',
			),
			array(
				'name'	=> __( 'Accent 7', $this->plugin_name  ),
				'slug'	=> 'accent-7',
				'color'	=> '#FEF4F4',
			),

			array(
				'name'	=> __( 'White', $this->plugin_name  ),
				'slug'	=> 'white',
				'color'	=> '#FDFFFC',
			),


		) );


	}

	/**
	 * Register the Custom Post Types & Taxonomies needed for ths site.
	 *
	 * @since    1.0.0
	 */	
	public function register_all_post_types(){


		$labels = array(
			'name'                  => _x( 'Work',  $this->plugin_name ),
			'singular_name'         => _x( 'Work', $this->plugin_name ),
			'menu_name'             => __( 'Work', $this->plugin_name ),
			'name_admin_bar'        => __( 'Work', $this->plugin_name ),
			'archives'              => __( 'Work Archives', $this->plugin_name ),
			'attributes'            => __( 'Work Attributes', $this->plugin_name ),
			'parent_item_colon'     => __( 'Parent Item:', $this->plugin_name ),
			'all_items'             => __( 'Work', $this->plugin_name ),
			'add_new_item'          => __( 'Add New Work', $this->plugin_name ),
			'add_new'               => __( 'Add Work', $this->plugin_name ),
			'new_item'              => __( 'New Work', $this->plugin_name ),
			'edit_item'             => __( 'Edit Work', $this->plugin_name ),
			'update_item'           => __( 'Update Work', $this->plugin_name ),
			'view_item'             => __( 'View Work', $this->plugin_name ),
			'view_items'            => __( 'View Work', $this->plugin_name ),
			'search_items'          => __( 'Search Work', $this->plugin_name ),
			'not_found'             => __( 'Not found', $this->plugin_name ),
			'not_found_in_trash'    => __( 'Not found in Trash', $this->plugin_name ),
			'featured_image'        => __( 'Featured Image', $this->plugin_name ),
			'set_featured_image'    => __( 'Set featured image', $this->plugin_name ),
			'remove_featured_image' => __( 'Remove featured image', $this->plugin_name ),
			'use_featured_image'    => __( 'Use as featured image', $this->plugin_name ),
			'insert_into_item'      => __( 'Insert into Event', $this->plugin_name ),
			'uploaded_to_this_item' => __( 'Uploaded to this Event', $this->plugin_name ),
			'items_list'            => __( 'Items list', $this->plugin_name ),
			'items_list_navigation' => __( 'Items list navigation', $this->plugin_name ),
			'filter_items_list'     => __( 'Filter Work', $this->plugin_name ),
		);
		$args = array(
			'label'                 => __( 'Work', $this->plugin_name ),
			'description'           => __( 'Work', $this->plugin_name ),
			'labels'                => $labels,
			'supports'              => array( 'title', "editor", 'thumbnail' ),
			'taxonomies'            => array(),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,			
			'show_in_rest'			=> true,
			"rewrite" => [ "slug" => "work", "with_front" => true ],
			'show_in_graphql' 		=> true,
			'graphql_single_name' => 'work',
			'graphql_plural_name' => 'works',

		);
		register_post_type( 'work', $args );		

	}

	public function allowed_blocks( $blocks ){

		return $blocks;

	}

	public function gumponents_posts_relationship_results( $posts ){


		$return = array();

		if( is_array( $posts ) ){

			foreach( $posts as $key => $value ){

				$image = get_the_post_thumbnail_url( $posts[$key]['id'], 'full' );

				$posts[$key]['value']->image = $image; 


			}

		}

		update_option( 'debug', $posts );

		return $posts;


	}
    
}