<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://noon.studio
 * @since      1.0.0
 *
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Noon_Studio_Plugin
 * @subpackage Noon_Studio_Plugin/public
 * @author     Studio Noon <matt@noon.studio>
 */
class Noon_Studio_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Noon_Studio_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Noon_Studio_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/noon-studio-plugin-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Noon_Studio_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Noon_Studio_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/noon-studio-plugin-public.js', array( 'jquery' ), $this->version, false );

	}

    public function register_cpts() {

        /**
		 * Post Type: Videos.
		 */

		$labels = [
			"name" => __( "Projects", "understrap-child" ),
			"singular_name" => __( "Project", "understrap-child" ),
		];

		$args = [
			"label" => __( "Projects", "understrap-child" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "projects", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-media-document",
			"supports" => [ "title", "editor", "thumbnail", "excerpt", "revisions", "author" ],
			"taxonomies" => [ "category", "post_tag" ],
		];

		register_post_type( "projects", $args );

    }

}
