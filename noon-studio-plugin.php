<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://noon.studio
 * @since             1.0.0
 * @package           Noon_Studio_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Noon Studio Plugin
 * Plugin URI:        noon.studio
 * Description:       This is the plugin we are working on.
 * Version:           1.0.0
 * Author:            Studio Noon
 * Author URI:        https://noon.studio
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       noon-studio-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'NOON_STUDIO_PLUGIN_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-noon-studio-plugin-activator.php
 */
function activate_noon_studio_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-noon-studio-plugin-activator.php';
	Noon_Studio_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-noon-studio-plugin-deactivator.php
 */
function deactivate_noon_studio_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-noon-studio-plugin-deactivator.php';
	Noon_Studio_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_noon_studio_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_noon_studio_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-noon-studio-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_noon_studio_plugin() {

	$plugin = new Noon_Studio_Plugin();
	$plugin->run();

}
run_noon_studio_plugin();
