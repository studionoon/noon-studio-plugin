<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function multi_block_cgb_block_assets() {
	// Styles.
	wp_enqueue_style(
		'multi_block-cgb-style-css', // Handle.
		plugins_url( 'blocks/build/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
} // End function multi_block_cgb_block_assets().

// Hook: Frontend assets.
//add_action( 'enqueue_block_assets', 'multi_block_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function multi_block_cgb_editor_assets() {
	// Scripts.
	wp_enqueue_script(
		'multi_block-cgb-block-js', // Handle.
		plugins_url( '/blocks/build/index.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-dom-ready', 'wp-edit-post', 'wp-hooks', 'gumponents' ), // Dependencies, defined above.
		filemtime( plugin_dir_path( __DIR__ ) . '/blocks/build/index.js' ) // Version: File modification time.
	);

	// Styles.
	wp_enqueue_style(
		'multi_block-cgb-block-editor-css', // Handle.
		plugins_url( '/blocks/build/index.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		filemtime( plugin_dir_path( __DIR__ ) . '/blocks/build/index.css' ) // Version: File modification time.
	);


} // End function multi_block_cgb_editor_assets().

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'multi_block_cgb_editor_assets' );

add_action( 'wp_enqueue_scripts', 'block_rendering' );
function block_rendering(){

	register_block_type( 'noon/partners', array(
        'api_version'       => 2,
        'render_callback'   => 'noon_partners_block',
        "attributes"        => []
    ) );
}

function noon_partners_block( $attr, $content ) {

    $attr['innerblocks'] = $content;

	ob_start();

		get_template_part( 'template-parts/noon', 'partners', $attr );

	return ob_get_clean();

}