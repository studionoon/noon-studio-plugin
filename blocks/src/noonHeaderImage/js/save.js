/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const { attributes: {  mediaURL, headerimage_heading, headerimage_title }, } = props;
    //  const { attributes } = props;
 
    const blockProps = useBlockProps.save();


 
     return (
        <div { ...blockProps }>

            <div className="container noon/coverimage parent-coverimage-in-editor">
                
                <div className="row">                
                    
                    <div className="coverimage-text-body col-6">

                        <div>

                            <h1>{headerimage_heading}</h1>
                            
                        </div>

                        <div>

                            <h2>{headerimage_title}</h2>

                        </div>

                        <div>

                            <InnerBlocks.Content />

                        </div>         

                    </div>

                    <div className="coverimage-image col-6">

                        <img
                            className=''
                            src={ mediaURL }
                            alt={ __( 'Cover Image', 'gutenberg-examples' ) }
                        />

                    </div>

                </div>

            </div>

        </div>
 
    );

};

export default Save;