/**
 * WordPress dependencies
 */

import { __ } from '@wordpress/i18n';
import { MediaUpload, InspectorControls,  useBlockProps, InnerBlocks, RichText } from '@wordpress/block-editor';
import { Button, FocalPointPicker, PanelBody } from '@wordpress/components';
 
const Edit = ( props ) => {
	const {
		attributes: { mediaID, mediaURL, focalPoint, headerimage_heading, headerimage_title },
		setAttributes,
	} = props;	
 
	const blockProps = useBlockProps();
 
	const onSelectImage = ( media ) => {
		setAttributes( {
			mediaURL: media.url,
			mediaID: media.id,
		} );
	};

    function onHeadingChange(changes) {
		// works very much like setState
		setAttributes({
			headerimage_heading: changes
		});
	}

    function onTitleChange(changes) {
		// works very much like setState
		setAttributes({
			headerimage_title: changes
		});
	}

    const ALLOWED_BLOCKS = [ 'core/paragraph', 'core/list', 'core/heading' ]

	// Focal Preview

	return (

		<div { ...blockProps }>

            <InspectorControls>

                <PanelBody title={ __( 'Media settings' ) }>
                    
                    <FocalPointPicker
                        label={ __( 'Focal point picker' ) }
                        url={ mediaURL }
                        value={ focalPoint }
                        onChange={ ( newFocalPoint ) =>
                            setAttributes( {
                                focalPoint: newFocalPoint,
                            } )
                        }
                    />
                    
                </PanelBody>	

            </InspectorControls>

            <div className="container noon/coverimage parent-coverimage-in-editor">
    
                <div className="row">

                    <div className="coverimage-image">

                        <RichText
                            tagName="h1"
                            value={ headerimage_heading }
                            onChange={onHeadingChange}
                            placeholder="Enter your heading here!"
                        />    

                        <RichText
                            tagName="h2"
                            value={ headerimage_title }
                            onChange={onTitleChange}
                            placeholder="Enter your title here!"
                        />                    

                        <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />            

                    </div>

                    <div className="coverimage-text-body">

                        <MediaUpload

                            onSelect={ onSelectImage }
                            allowedTypes="image"
                            value={ mediaID }
                            render={ ( { open } ) => (

                                <Button

                                    className={
                                        mediaID ? 'image-button' : 'button button-large'
                                    }

                                    onClick={ open }
                                >

                                    { ! mediaID ? (

                                        __( 'Upload Image', 'gutenberg-examples' )

                                    ) : (

                                        <img
                                            className='background_images'
                                            src={ mediaURL }
                                            alt={ __(
                                                'Upload Cover Image',
                                                'gutenberg-examples'
                                            ) }
                                        />

                                    ) }

                                </Button>

                            ) }

                        />

                    </div>

                </div>

            </div>

		</div>

	);

};

export default Edit;
 