/**
 * External Dependencies
 */
 import classnames from 'classnames';
 import { assign } from 'lodash';
 import './editor.scss';

 /**
  * WordPress Dependencies
  */
 const { __ } = wp.i18n;
 const { addFilter } = wp.hooks;
 const { Fragment }	= wp.element;
 const { InspectorControls }	= wp.blockEditor;
 const { createHigherOrderComponent } = wp.compose;
 const { PanelBody, PanelRow, RangeControl, ToggleControl, CheckboxControl } = wp.components;

 //restrict to specific block names
 const allowedBlocks = [ 'core/list' ];
 
 /**
  * Add custom attribute for mobile visibility.
  *
  * @param {Object} settings Settings for the block.
  *
  * @return {Object} settings Modified settings.
  */

 
 /**
  * Add mobile visibility controls on Advanced Block Panel.
  *
  * @param {function} BlockEdit Block edit component.
  *
  * @return {function} BlockEdit Modified block edit component.
  */
 const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
   return ( props ) => {
 
     const {
       name,
       attributes: { hideTickIcon, addTitle, addIntro, listColumns, showColumnsOnMobile, addListCheckMark },
       setAttributes,
       
       isSelected,
     } = props;
 
    //  const {
    //    addListCheckMark,
    //  } = attributes;
   

     return (

       <Fragment>

           
         <BlockEdit {...props} />



         { isSelected && allowedBlocks.includes( name ) &&

         <>

                <InspectorControls>
					<PanelBody title={ __( 'Layout Settings', 'lazy-lists' ) }>
						<RangeControl
							label={ __( 'Columns', 'lazy-lists' ) }
							value={ listColumns || 1 }
							onChange={ ( newColumns ) => {
								setAttributes( { listColumns: newColumns } );
							} }
							min={ 1 }
							max={ 5 }
						/>
						<ToggleControl
							label={ __(
								'Show columns on mobile',
								'lazy-lists',
							) }
							checked={ showColumnsOnMobile }
							onChange={ () =>
								setAttributes( {
									showColumnsOnMobile: ! showColumnsOnMobile,
								} )
							}
						/>

                        <ToggleControl
                            label={ __( 'Want a boring list?' ) }
                            checked={ !! addListCheckMark }
                            onChange={ () => setAttributes( {  addListCheckMark: ! addListCheckMark } ) }
                            help={ !! addListCheckMark ? __( 'No Check Mark' ) : __( 'Check Mark' ) }
                        />

                        <ToggleControl
                            label={ __( 'Hide Tick icon' ) }
                            checked={ !! hideTickIcon }
                            onChange={ () => setAttributes( {  hideTickIcon: ! hideTickIcon } ) }
                            help={ !! hideTickIcon ? __( 'No Icon' ) : __( 'No Icon' ) }
                        />

					</PanelBody>

                    <PanelBody
						title="Column Title options"
						initialOpen={true}
					>

						<PanelRow>

							<CheckboxControl
								label="Add Title"
								checked={addTitle}
								onChange={(newval) => setAttributes({ addTitle: newval })}
							/>

						</PanelRow>

						{ addTitle && 

							<PanelRow>

								<SelectControl
									label={ __( 'Units' ) }
									value={ titleSize }
									onChange={ ( value ) => setAttributes({ titleSize: value })}
									options={ [
										{ label: 'Standard', value: 'standard' },
										{ label: 'Large', value: 'large' }

									] }
								/>

							</PanelRow>

						}
						
						<PanelRow>

							<CheckboxControl
								label="Add Introduction"
								checked={addIntro}
								onChange={(newval) => setAttributes({ addIntro: newval })}
							/>

						</PanelRow>

					</PanelBody>
				</InspectorControls>

           </>
         }
 
       </Fragment>
     );
   };
 }, 'withAdvancedControls');

 function addAttributes( settings ) {
   
    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
    
     settings.attributes = Object.assign( settings.attributes, {
         addListCheckMark:{ 
             type: 'boolean',
             default: false,
         },
         listColumns: {
             type: 'number',
             default: 1,
         },
         showColumnsOnMobile: {
             type: 'boolean',
             default: false,
         },
         hideTickIcon: {
             type: 'boolean',
             default: false,
         }
     });
       settings.supports.color = false; 
      
     }
  
    return settings;
  }

 const addColumnsClassToEditor = createHigherOrderComponent(
    ( BlockListBlock ) => {
        return ( props ) => {
            const {
                name,
                block: {
                    attributes: { listColumns },
                },
            } = props;

            if ( name !== 'core/list' ) {
                return <BlockListBlock { ...props } />;
            }

            return (
                
                <BlockListBlock
                    { ...props }
                    className={ `wp-block-list--${ listColumns }-columns` }
                />

            );
        };
    },
    'addColumnsClassToEditor',
);
 
 /**
  * Add custom element class in save element.
  *
  * @param {Object} extraProps     Block element.
  * @param {Object} blockType      Blocks object.
  * @param {Object} attributes     Blocks attributes.
  *
  * @return {Object} extraProps Modified block element.
  */
 function applyExtraClass( extraProps, blockType, attributes ) {
 
   const { addListCheckMark, listColumns, showColumnsOnMobile } = attributes;
   
   if ( typeof addListCheckMark !== 'undefined' && !addListCheckMark && allowedBlocks.includes( blockType.name ) ) {
     extraProps.className = classnames( extraProps.className, 'noon-check-mark' );
   }

   return extraProps;
 }

const addProps = ( props, blockType, { listColumns, showColumnsOnMobile } ) => {
    if ( blockType.name !== 'core/list' ) {
        return props;
    }

    const className = classnames( {
        'wp-block-list--show-columns-on-mobile': showColumnsOnMobile,
        [ `wp-block-list--${ listColumns }-columns` ]: Number( listColumns ) > 1,
    } );

    if ( ! className ) {
        return props;
    }

    return assign( props, { className } );
};
 
//add filters

addFilter(
    'blocks.registerBlockType',
    'editorskit/custom-attributes',
    addAttributes
);
 
addFilter(
    'editor.BlockEdit',
    'editorskit/custom-advanced-control',
    withAdvancedControls
);
 
// addFilter(
//     'blocks.getSaveContent.extraProps',
//     'editorskit/applyExtraClass',
//     applyExtraClass
// );

addFilter(
    'blocks.getSaveContent.extraProps',
    'sorta-brilliant/lazy-lists',
    addProps,
);
addFilter(
    'editor.BlockListBlock',
    'sorta-brilliant/lazy-lists',
    addColumnsClassToEditor,
);