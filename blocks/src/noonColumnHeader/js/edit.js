/**
 * WordPress dependencies
 */

 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps, RichText, InspectorControls } from '@wordpress/block-editor';
 import { PanelBody, PanelRow, CheckboxControl, SelectControl } from '@wordpress/components';

 const Edit = ( props ) => {


	const { setAttributes, attributes } = props;
	const blockProps = useBlockProps();

	// const ALLOWED_BLOCKS = [  'noon/accordian-parent', 'noon/carousel', 'core/media-text', 'core/gallery', 'core/columns' ]




	
	
	 return (

		<>
			<InspectorControls>

					<PanelBody
						title="Group Options"
						initialOpen={true}
					>

						<PanelRow>

							<CheckboxControl
								label="Add Title"
								checked={attributes.addTitle}
								onChange={(newval) => setAttributes({ addTitle: newval })}
							/>

						</PanelRow>

						{ attributes.addTitle && 

							<PanelRow>

								<SelectControl
									label={ __( 'Units' ) }
									value={ attributes.titleSize }
									onChange={ ( value ) => setAttributes({ titleSize: value })}
									options={ [
										{ label: 'Standard', value: 'standard' },
										{ label: 'Large', value: 'large' }

									] }
								/>

							</PanelRow>

						}
						
						<PanelRow>

							<CheckboxControl
								label="Add Introduction"
								checked={attributes.addIntro}
								onChange={(newval) => setAttributes({ addIntro: newval })}
							/>

						</PanelRow>

					</PanelBody>

			</InspectorControls>
 
			 <div className={`wrapper p-0`}>
 
				 <div className="container">

					<div class="row">

						<div class="col-12">

							{ attributes.addTitle && 

								<RichText
									className={attributes.titleSize}
									tagName="h2"
									value={attributes.groupTitle}
									onChange={ ( value ) =>
										setAttributes( {
											groupTitle: value,
										} )
									}
									placeholder="Title"
								/>

							}

							{ attributes.addIntro && 

								<RichText
									tagName="p"
									value={attributes.groupIntro}
									onChange={ ( value ) =>
										setAttributes( {
											groupIntro: value,
										} )
									}
									placeholder="Intro"
								/>

							}

						</div>
						
					</div>

					{/* <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } /> */}

				 </div>
 
			 </div>
 
		 </>
	 );
 };
 export default Edit;
 