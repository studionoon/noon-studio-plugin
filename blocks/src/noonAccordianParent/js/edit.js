/**
* WordPress dependencies
*/

import { __ } from '@wordpress/i18n';
import { InnerBlocks, useBlockProps,  } from '@wordpress/block-editor';
const { select, dispatch } = wp.data;
const { InspectorControls, PanelColorSettings, getColorObjectByColorValue } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker, RangeControl } = wp.components;

 
const Edit = ( props ) => {
	const {
		attributes,
		setAttributes,
		clientId,
	} = props;	
 
	const blockProps = useBlockProps();
	const ALLOWED_BLOCKS = [ 'noon/accordian-child' ]

	// Varibles 
	const accordian_id = 'acc_' + clientId;


    function onChangeBGColor ( value ) {

        let colorName = '';
        if( value ) {

            const settings = select( 'core/editor' ).getEditorSettings();
            const colorObject = getColorObjectByColorValue( settings.colors, value );
            if( colorObject ) {

				props.setAttributes({colorName: colorObject.slug });

            }

        }
       	props.setAttributes({color: value });

    }

    function onChangeTextColor ( value ) {

        let colorName = '';
        if( value ) {

            const settings = select( 'core/editor' ).getEditorSettings();
            const colorObject = getColorObjectByColorValue( settings.colors, value );
            if( colorObject ) {

				props.setAttributes({textColorName: colorObject.slug });

            }

        }
       	props.setAttributes({textColor: value });

    }

	if ( ! attributes.uniqueID ) {
        setAttributes( { uniqueID: clientId } );
    }

	onaccordianDisplayChange( attributes.accordianDisplay );
    function onaccordianDisplayChange ( value ) {

		setAttributes({ accordianDisplay: value })

		select('core/editor').getBlocksByClientId(clientId)[0].innerBlocks.forEach(function (block, index) {
			dispatch('core/editor').updateBlockAttributes(block.clientId, { style_type: value })
			dispatch('core/editor').updateBlockAttributes(block.clientId, { accordian_id:  accordian_id })
			dispatch('core/editor').updateBlockAttributes(block.clientId, { addNumbers:  attributes.addNumbers })
			dispatch('core/editor').updateBlockAttributes(block.clientId, { count:  index })
		})

    }



	return (
		// <div { ...blockProps }> 

		<>

                <InspectorControls>

					<PanelBody
						title="Accordian Settings"
						initialOpen={false}
					>

						<PanelRow>

							<CheckboxControl
								label="Add numbers"
								checked={attributes.addNumbers}
								onChange={(newval) => setAttributes({ addNumbers: newval })}
							/>
							
						</PanelRow>


					</PanelBody>


					<PanelColorSettings 
						title={__('Color settings')}
						colorSettings={[
							{
								value: attributes.color,
								onChange: ( colorValue ) => onChangeBGColor( colorValue ),
								label: __( 'Background Color' ),
							},
							{
								value: attributes.textColor,
								onChange: ( colorValue ) => onChangeTextColor( colorValue ),
								label: __( 'Text Color' ),
							},
						]}
					/>

					<PanelBody
						title="Column Settings"
						initialOpen={false}
					>

						<PanelRow>

							<SelectControl
								label="Display"
								value={attributes.accordianDisplay}
								options={[
									{label: "Inline", value: 'inline'},
									{label: "Columns", value: 'columns'},
									{label: "With Images", value: 'with-images'},
								]}
								onChange= { ( newval ) =>  onaccordianDisplayChange( newval ) }
							/>
						
						</PanelRow>

						<PanelRow>

							<CheckboxControl
								label="Wrap Block in a wrapper"
								checked={attributes.addWrapper}
								onChange={(newval) => setAttributes({ addWrapper: newval })}
							/>

						</PanelRow>

                    { attributes.accordianDisplay == 'columns' &&

						<PanelRow>
							<SelectControl
								label="Accordian breakpoint......."
								value={attributes.accordion}
								options={[
									{label: "XS", value: 'xs'},
									{label: "SM", value: 'sm'},
									{label: "MD", value: 'md'},
									{label: "LG", value: 'lg'},
									{label: "XL", value: 'xl'},
									{label: "XXL", value: 'xxl'},
									{label: "None", value: 'none'},
								]}
								onChange={(newval) => setAttributes({ accordion: newval })}
							/>
						</PanelRow>

                    }

                        <PanelRow>
							<CheckboxControl
								label="Default Open"
								checked={attributes.defaultOpen}
								onChange={(newval) => setAttributes({ defaultOpen: newval })}
							/>
						</PanelRow>
					</PanelBody>
				</InspectorControls>

				<div class="accordion" id={ accordian_id }>

					<InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />

				</div>

			</>
 
		// </div>
	);
};
export default Edit;
 