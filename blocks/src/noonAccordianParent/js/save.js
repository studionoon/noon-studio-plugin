/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const {
         attributes,
     } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
        <div { ...blockProps }>
 
             <div className="container noon-accordian-parent">
 
                <div className="row">

                    {/* <div className="accordion" id="accordionExample"> */}

                    <div className={"btn-group pull-right "}>
 
                        <InnerBlocks.Content />

                    </div>

                </div>

             </div>
             
        </div>
 
     );
 };
 export default Save;