/**
 * WordPress dependencies
 */

import { __ } from '@wordpress/i18n';
import { InspectorControls, InnerBlocks, useBlockProps, RichText } from '@wordpress/block-editor';
const { PanelBody, PanelRow, RangeControl, TextControl, SelectControl  } = wp.components;
import {  select, dispatch } from '@wordpress/data';


 const Edit = ( props ) => {
	 const {
		 attributes : { uniqueID, slide_in_view_xs, slide_in_view_sm, slide_in_view_md, slide_in_view_lg, slide_in_view_xl, slide_in_view_xxl, imageBreakpoints, image_height_xs, image_height_sm, image_height_md, image_height_lg, image_height_xl, image_height_xxl, imageBreakpointUnits, image_unit_xs, image_unit_sm, image_unit_md, image_unit_lg,  image_unit_xl, image_unit_xxl, breakpoints, groupTitle, groupIntro, groupIntro2, groupIntro3, groupCTA },
		 setAttributes,
		 clientId,
	 } = props;	
 
	const blockProps = useBlockProps();
	const ALLOWED_BLOCKS = [ 'noon/carousel-child' ]

	 var breakpoint = breakpoints;
	 var imageBreakpoint = imageBreakpoints;
	 var imageBreakpointUnit = imageBreakpointUnits;


	 if ( ! uniqueID ) {
        setAttributes( { uniqueID: clientId } );
    }



	 // Breakpoints
	 function onBreakpointChange( value, size ) {

		if( typeof breakpoint === 'object' && breakpoint !== null ){

			Object.assign(breakpoint, { [size] : value} );

		} else {

			breakpoint = new Object( { [size] : value } )
		}

		// works very much like setState
		setAttributes( { breakpoints: breakpoint } );

		
	}

	// Breakpoints
	function onImageBreakpointChange( value, size ) {

		if( typeof imageBreakpoint === 'object' && imageBreakpoint !== null ){

			Object.assign(imageBreakpoint, { [size] : value} );

		} else {

			imageBreakpoint = new Object( { [size] : value } )
		}

		// works very much like setState
		setAttributes( { imageBreakpoints: imageBreakpoint } );

		
	}

	// Breakpoints
	function onImageBreakpointUnitChange( value, size ) {

		if( typeof imageBreakpointUnit === 'object' && imageBreakpointUnit !== null ){

			Object.assign(imageBreakpointUnit, { [size] : value} );

		} else {

			imageBreakpointUnit = new Object( { [size] : value } )
		}

		// works very much like setState
		setAttributes( { imageBreakpointUnits: imageBreakpointUnit } );

	
	}    
    
    select('core/editor').getBlocksByClientId(clientId)[0].innerBlocks.forEach(function (block) {

        console.log( block );

        dispatch('core/editor').updateBlockAttributes(block.clientId, { tester: 'wooo' })
    })


	return (

		<div>

		<InspectorControls>

			<PanelBody
				title="Slide Breakpoints"
				initialOpen={false}
			>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides XS"
						value={ slide_in_view_xs }
						onChange={ ( value ) => {  onBreakpointChange( value, 'XS' ); setAttributes( { slide_in_view_xs: value } ); }  }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides SM"
						value={ slide_in_view_sm }
						onChange={ ( value ) => { onBreakpointChange( value, 'SM' ); setAttributes( { slide_in_view_sm: value } ); } }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides MD"
						value={ slide_in_view_md }
						onChange={ ( value ) => { onBreakpointChange( value, 'MD' ); setAttributes( { slide_in_view_md: value } ); } }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides LG"
						value={ slide_in_view_lg }
						onChange={ ( value ) => { onBreakpointChange( value, 'LG' ); setAttributes( { slide_in_view_lg: value } ); } }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides XL"
						value={ slide_in_view_xl }
						onChange={ ( value ) => { onBreakpointChange( value, 'XL' ); setAttributes( { slide_in_view_xl: value } ); } }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

				<PanelRow>

					<RangeControl
						allowReset={true}
						label="Slides XXL"
						value={ slide_in_view_xxl }
						onChange={ ( value ) => { onBreakpointChange( value, 'XXL' ); setAttributes( { slide_in_view_xxl: value } ); } }
						min={ 1 }
						max={ 10 }
					/>

				</PanelRow>

			</PanelBody>

			<PanelBody
				title="Slide Heights"
				initialOpen={false}
				className="image_height"
			>

				<PanelRow>

                    <TextControl 
                        label="Image height at XS"
                        value={ image_height_xs }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'XS' ); setAttributes( { image_height_xs: value } ); } }
                        type="number"
                    />

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_xs }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XS' ); setAttributes( { image_unit_xs: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

				<PanelRow>

                    <TextControl 
                        label="Image height at SM"
                        value={ image_height_sm }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'SM' ); setAttributes( { image_height_sm: value } ); } }
                        type="number"
                    />

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_sm }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'SM' ); setAttributes( { image_unit_sm: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

				<PanelRow>

                    <TextControl 
                        label="Image height at MD"
                        value={ image_height_md }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'MD' ); setAttributes( { image_height_md: value } ); } }
                        type="number"
                    />

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_md }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'MD' ); setAttributes( { image_unit_md: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

				<PanelRow>

                    <TextControl 
                        label="Image height at LG"
                        value={ image_height_lg }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'LG' ); setAttributes( { image_height_lg: value } ); } }
                        type="number"
                    /> 

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_lg }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'LG' ); setAttributes( { image_unit_lg: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

				<PanelRow>

                    <TextControl 
                        label="Image height at XL"
                        value={ image_height_xl }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'XL' ); setAttributes( { image_height_xl: value } ); } }
                        type="number"
                    />

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_xl }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XL' ); setAttributes( { image_unit_xl: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

				<PanelRow>

                    <TextControl 
                        label="Image height at XXL"
                        value={ image_height_xxl }
                        onChange={ ( value ) => { onImageBreakpointChange( value, 'XXL' ); setAttributes( { image_height_xxl: value } ); } }
                        type="number"
                    />

                    <SelectControl
                        label={ __( 'Units' ) }
                        value={ image_unit_xxl }
                        onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XXL' ); setAttributes( { image_unit_xxl: value } ); } }
                        options={ [
                            { label: 'px', value: 'px' },
                            { label: 'vh', value: 'vh' },
                            { label: 'vw', value: 'vw' },
                            { label: 'rem', value: 'rem' },
                        ] }
                    />

				</PanelRow>

			</PanelBody>

		</InspectorControls>

		    <div className="container noon/carousel-parent parent-carousel-in-editor">

			    <div className="row">

				    <div className="col-12">

                        <RichText
                            tagName="h3"
                            value={groupTitle}
                            onChange={ ( value ) =>
                                setAttributes( {
                                    groupTitle: value,
                                } )
                            }
                            placeholder="Title"
                        />

                        <RichText
                            tagName="h4"
                            value={groupIntro}
                            onChange={ ( value ) =>
                                setAttributes( {
                                    groupIntro: value,
                                } )
                            }
                            placeholder="Sub Heading"
                        />

                        {/* <RichText
                            tagName="p"
                            value={groupIntro2}
                            onChange={ ( value ) =>
                                setAttributes( {
                                    groupIntro2: value,
                                } )
                            }
                            placeholder="Column Left"
                        />

                        <RichText
                            tagName="p"
                            value={groupIntro3}
                            onChange={ ( value ) =>
                                setAttributes( {
                                    groupIntro3: value,
                                } )
                            }
                            placeholder="Column Right"
                        /> */}

				    </div>

			    </div>



                <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />


                <div className="row">

                    <div className="col-12">

                        <RichText
                            tagName="p"
                            value={groupCTA}
                            onChange={ ( value ) =>
                                setAttributes( {
                                    groupCTA: value,
                                } )
                            }
                            placeholder="Call to action"
                        />

                    </div>

                </div>

		    </div>

		</div>
 
	);
};
export default Edit;
 