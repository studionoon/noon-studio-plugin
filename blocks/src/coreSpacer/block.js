/**
 * External Dependencies
 */
 import classnames from 'classnames';
 import { assign } from 'lodash';

 /**
  * WordPress Dependencies
  */
 const { __ } = wp.i18n;
 const { addFilter } = wp.hooks;
 const { Fragment }	= wp.element;
 const { InspectorControls }	= wp.blockEditor;
 const { createHigherOrderComponent } = wp.compose;
 const { PanelBody, PanelRow, RangeControl, ToggleControl, CheckboxControl } = wp.components;

 //restrict to specific block names
 const allowedBlocks = [ 'core/spacer' ];
 /**
  * Add custom attribute for mobile visibility.
  *
  * @param {Object} settings Settings for the block.
  *
  * @return {Object} settings Modified settings.
  */

 
 /**
  * Add mobile visibility controls on Advanced Block Panel.
  *
  * @param {function} BlockEdit Block edit component.
  *
  * @return {function} BlockEdit Modified block edit component.
  */
 const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
   return ( props ) => {
 
     const {
       name,
       attributes: { heightXS, heightSM, heightMD, heightLG, heightXL, heightXXL},
       setAttributes,
       isSelected,
     } = props;
 


     return (

       <Fragment>

           
         <BlockEdit {...props} />

         { isSelected && allowedBlocks.includes( name ) &&

         <>

                <InspectorControls>

					<PanelBody title={ __( 'Responsive Settings') }>

                        <RangeControl
                            allowReset={true}
                            label="Height in pixels XS"
                            value={ heightXS }
                            onChange={ ( value ) => setAttributes( { heightXS: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />
                        <RangeControl
                            allowReset={true}
                            label="Height in pixels SM"
                            value={ heightSM }
                            onChange={ ( value ) => setAttributes( { heightSM: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />
                        <RangeControl
                            allowReset={true}
                            label="Height in pixels MD"
                            value={ heightMD }
                            onChange={ ( value ) => setAttributes( { heightMD: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />
                        <RangeControl
                            allowReset={true}
                            label="Height in pixels LG"
                            value={ heightLG }
                            onChange={ ( value ) => setAttributes( { heightLG: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />
                        <RangeControl
                            allowReset={true}
                            label="Height in pixels XL"
                            value={ heightXL }
                            onChange={ ( value ) => setAttributes( { heightXL: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />
                        <RangeControl
                            allowReset={true}
                            label="Height in pixels XXL"
                            value={ heightXXL }
                            onChange={ ( value ) => setAttributes( { heightXXL: value } )  }
                            min={ 1 }
                            max={ 300 }
                        />

					</PanelBody>

				</InspectorControls>

           </>
         }
 
       </Fragment>
     );
   };
 }, 'withAdvancedControls');

function addAttributes( settings ) {
   
    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
    
     settings.attributes = Object.assign( settings.attributes, {

        heightXS: {
			type:'integer',
		},
       heightSM: {
			type:'integer',
		},
       heightMD: {
			type:'integer',
		},
       heightLG: {
			type:'integer',
		},
       heightXL: {
			type:'integer',
		},
       heightXXL: {
			type:'integer',
		},
     });
      
     }
  
    return settings;
  }

 
//add filters
addFilter(
    'blocks.registerBlockType',
    'noon/spacer-attributes',
    addAttributes
);
 
addFilter(
    'editor.BlockEdit',
    'noon/spacer-advanced-control',
    withAdvancedControls
);
