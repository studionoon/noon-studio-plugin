/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const { attributes } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
         <div { ...blockProps }>
 
 
             <div className="container-fluid position-relative">
 
                 <div className="row">
 
                     <div className="container noon-accordian-child">
 
                        <div class="accordion-item">
 
                            <h3>{attributes.acccordian_header}</h3>

                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <InnerBlocks.Content />
                                </div>
                            </div>

 
                        </div>
                    </div>
 
                 </div>
 
             </div>
             
         </div>
 
     );
 };
 export default Save;