/**
* WordPress dependencies
*/


import { __ } from '@wordpress/i18n';
import { InnerBlocks, useBlockProps, MediaUpload, MediaUploadCheck } from '@wordpress/block-editor';
import { Button, FocalPointPicker } from '@wordpress/components';
const { RichText, InspectorControls } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker, RangeControl } = wp.components;

// import "../blocks/node_modules/bootstrap/js/src/collapse.js"

const Edit = ( props ) => {


	const { setAttributes, attributes, mediaID, focalPoint, mediaURL, titleTag, titleStyle, clientId } = props;
 
	const blockProps = useBlockProps();

    //restrict to specific block names
    const ALLOWED_BLOCKS = [ 'core/paragraph', 'core/list', 'core/heading' ]
 
	function onHeaderChange(changes) {
		// works very much like setState
		setAttributes({
			acccordian_header: changes
		});
	}

    const onSelectImage_1 = ( media ) => {
        setAttributes( {
            mediaURL_1: media.url,
            mediaID_1: media.id,
        } );
    };

    const onSelectImage_2 = ( media ) => {
        setAttributes( {
            mediaURL_2: media.url,
            mediaID_2: media.id,
        } );
    };

    const accordian_header = 'header_' + clientId;
    const accordian_collapse = 'header_' + clientId;

    let numbers = attributes.count + 1;


	
	return (

        <> 

		{/* <div { ...blockProps }> */}

                <InspectorControls>

                <PanelBody
                    title="Typography"
                    initialOpen={false}
				>
						<PanelRow>

                            <SelectControl
                                label={ __( 'Title Tag' ) }
                                value={ attributes.titleTag }
                                onChange={ ( value ) => setAttributes({ titleTag: value })}
                                options={ [
                                    { label: 'H2', value: 'h2' },
                                    { label: 'H3', value: 'h3' },
                                    { label: 'H4', value: 'h4' },
                                    { label: 'H5', value: 'h5' },
                                    { label: 'H6', value: 'h6' }
                                ] }
                            />

						</PanelRow>
						<PanelRow>

                            <SelectControl
                                label={ __( 'Title Style' ) }
                                value={ attributes.titleStyle }
                                onChange={ ( value ) => setAttributes({ titleStyle: value })}
                                options={ [
                                    { label: '', value: '' },
                                    { label: 'H2', value: 'h2' },
                                    { label: 'H3', value: 'h3' },
                                    { label: 'H4', value: 'h4' },
                                    { label: 'H5', value: 'h5' },
                                    { label: 'H6', value: 'h6' }
                                ] }
                            />

						</PanelRow>

                </PanelBody>	
					

                { attributes.style_type == "columns" && 

					<PanelBody
						title="Set Breakpoint sizes"
						initialOpen={false}
					>
						<PanelRow>
                        <RangeControl
                            label="Mobile (XS) Columns"
                            value={ attributes.columnsXS }
                            onChange={ ( newval ) => setAttributes({ columnsXS: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
						<PanelRow>
                        <RangeControl
                            label="Mobile (SM) Columns"
                            value={ attributes.columnsSM }
                            onChange={ ( newval ) => setAttributes({ columnsSM: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
						<PanelRow>
                        <RangeControl
                            label="Mobile (MD) Columns"
                            value={ attributes.columnsMD }
                            onChange={ ( newval ) => setAttributes({ columnsMD: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
						<PanelRow>
                        <RangeControl
                            label="Mobile (LG) Columns"
                            value={ attributes.columnsLG }
                            onChange={ ( newval ) => setAttributes({ columnsLG: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
						<PanelRow>
                        <RangeControl
                            label="Mobile (XL) Columns"
                            value={ attributes.columnsXL }
                            onChange={ ( newval ) => setAttributes({ columnsXL: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
						<PanelRow>
                        <RangeControl
                            label="Mobile (XXL) Columns"
                            value={ attributes.columnsXXL }
                            onChange={ ( newval ) => setAttributes({ columnsXXL: newval })}
                            min={ 0 }
                            max={ 12 }
                        />
						</PanelRow>
					</PanelBody>

                }

                { attributes.style_type == "with-images" && 


                        <PanelBody
                        title="Media Upload"
                        initialOpen={true}
                        >
                            <PanelRow>

                                <MediaUpload
                                onSelect={ onSelectImage_1 }
                                allowedTypes="image"
                                value={ attributes.mediaID_1 }
                                render={ (  { open }  ) => (
                                    <Button
                                        className={
                                            attributes.mediaID_1 ? 'image-button' : 'button button-large'
                                        }
                                        onClick={ open }
                                    >
                                        
                                    {/* { __( 'Upload Left Image', 'gutenberg-examples' ) } */}

                                    { ! attributes.mediaID_1 ? (
                                                    __( 'Upload Left Image', 'Upload Left Image' )
                                                ) : (
                                                    <div class="upload-button-images">
                                                        <p>Uploaded right image</p>
                                                    <img
                                                        className='images-for-buttons'
                                                        src={ attributes.mediaURL_1 }
                                                        alt={ __(
                                                            'Upload Left Image',
                                                            'gutenberg-examples'
                                                        ) }
                                                    />
                                                    </div>
                                                ) }

                                    </Button>
                                ) }
                                />
                            </PanelRow>
                            <PanelRow>

                                <MediaUpload
                                onSelect={ onSelectImage_2 }
                                allowedTypes="image"
                                value={ attributes.mediaID_2 }
                                render={ (  { open }  ) => (
                                    <Button
                                        className={
                                            attributes.mediaID_2 ? 'image-button' : 'button button-large'
                                        }
                                        onClick={ open }
                                    >
                                        
                                    {/* { __( 'Upload Right Image', 'gutenberg-examples' ) } */}

                                    { ! attributes.mediaID_1 ? (
                                                    __( 'Upload Right Image', 'Upload Left Image' )
                                                ) : (
                                                    <div class="upload-button-images">
                                                        <p>Uploaded right image</p>
                                                    <img
                                                        className='images-for-buttons'
                                                        src={ attributes.mediaURL_2 }
                                                        alt={ __(
                                                            'Upload Right Image',
                                                            'gutenberg-examples'
                                                        ) }
                                                    />
                                                    </div>
                                                ) }

                                    </Button>
                                ) }
                                />
                            </PanelRow>
                            
                        </PanelBody>

                        
                }

			</InspectorControls>

              
            { attributes.style_type == "with-images" && 
                <>

                    <div class="row">

                        <div class="col-12 col-md-6">
                            
                            <div class="accordion-item">
                                <h2 class="accordion-header" id={accordian_header} >
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={'#' + accordian_collapse } aria-expanded="true" aria-controls={accordian_collapse}>

                                        { attributes.addNumbers == true  && <span class="number">{ ('0' + numbers).slice(-2) } </span> } 

                                        <RichText
                                            value={attributes.acccordian_header}
                                            onChange={onHeaderChange}
                                            placeholder="Enter your text here!"
                                        />
                                    </button>
                                </h2>
                                <div id={accordian_collapse} class="accordion-collapse" aria-labelledby={accordian_header} data-bs-parent={ '#' + attributes.accordian_id }>
                                    <div class="accordion-body">
                                    <InnerBlocks  allowedBlocks={ ALLOWED_BLOCKS } />                        
                                    </div>
                                </div>
                            </div>
                            
                        </div> 

                        <div class="col-12 col-md-6">

                            <div class="row">

                                <div class="col">

                                <MediaUpload
                                        onSelect={ onSelectImage_1 }
                                        allowedTypes="image"
                                        value={ attributes.mediaID_1 }
                                        render={ ( { open } ) => (
                                            <Button
                                                className={
                                                    attributes.mediaID_1 ? 'image-button' : 'button button-large'
                                                }
                                                onClick={ open }
                                            >
                                                { ! attributes.mediaID_1 ? (
                                                    __( 'Upload Left Image', 'gutenberg-examples' )
                                                ) : (
                                                    <img
                                                        className='background_images'
                                                        src={ attributes.mediaURL_1 }
                                                        alt={ __(
                                                            'Upload Right Image',
                                                            'gutenberg-examples'
                                                        ) }
                                                    />
                                                ) }
                                            </Button>
                                        ) }
                                    />

                                    {/* { attributes.mediaID_1 ? <img
                                                            className='background_images'
                                                            src={ attributes.mediaURL_1 }
                                                            alt={ __(
                                                                'Upload Recipe Image',
                                                                'gutenberg-examples'
                                                            ) }
                                                        /> : '' } */}

                                </div>
                                
                                <div class="col">
                                    

                                    <MediaUpload
                                        onSelect={ onSelectImage_2 }
                                        allowedTypes="image"
                                        value={ attributes.mediaID_2 }
                                        render={ ( { open } ) => (
                                            <Button
                                                className={
                                                    attributes.mediaID_2 ? 'image-button' : 'button button-large'
                                                }
                                                onClick={ open }
                                            >
                                                { ! attributes.mediaID_2 ? (
                                                    __( 'Upload Right Image', 'gutenberg-examples' )
                                                ) : (
                                                    <img
                                                        className='background_images'
                                                        src={ attributes.mediaURL_2 }
                                                        alt={ __(
                                                            'Upload Right Image',
                                                            'gutenberg-examples'
                                                        ) }
                                                    />
                                                ) }
                                            </Button>
                                        ) }
                                    />

                                </div>

                            </div>

                        </div>

                    </div>

                </>
                
            }
             
                { attributes.style_type != "with-images" && 


                <>
                    
                    <div class="col-12">

                        <div class="accordion-item ">
                            <h2 class="accordion-header collapse" id={accordian_header} >
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={'#' + accordian_collapse } aria-expanded="true" aria-controls={accordian_collapse}>

                                    { attributes.addNumbers == true  && <span class="number">{ ('0' + numbers).slice(-2) } </span> } 

                                    <RichText
                                        value={attributes.acccordian_header}
                                        onChange={onHeaderChange}
                                        placeholder="Enter your text here!"
                                    />
                                </button>
                            </h2>
                            <div id={accordian_collapse} class="accordion-collapse collapse" aria-labelledby={accordian_header} data-bs-parent={ '#' + attributes.accordian_id }>
                                <div class="accordion-body">
                                <InnerBlocks  allowedBlocks={ ALLOWED_BLOCKS } />                        
                                </div>
                            </div>
                        </div>
                        
                    </div> 

                </>

            }


        {/* </div> */}

        </>
	 );
 };
 export default Edit;
 