/**
 * WordPress dependencies
 */

 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps, RichText, InspectorControls } from '@wordpress/block-editor';
 import { PanelBody, PanelRow, CheckboxControl, SelectControl } from '@wordpress/components';

 const Edit = ( props ) => {


	const { setAttributes, attributes } = props;
	const blockProps = useBlockProps();

	const ALLOWED_BLOCKS = [ 'core/gallery' ]
	
	 return (

		<>
			<InspectorControls>

					<PanelBody
						title="Group Options"
						initialOpen={true}
					>

						<PanelRow>

							<CheckboxControl
								label="Add Title"
								checked={attributes.addTitle}
								onChange={(newval) => setAttributes({ addTitle: newval })}
							/>

						</PanelRow>

						{ attributes.addTitle && 

							<PanelRow>

								<SelectControl
									label={ __( 'Units' ) }
									value={ attributes.titleSize }
									onChange={ ( value ) => setAttributes({ titleSize: value })}
									options={ [
										{ label: 'Standard', value: 'standard' },
										{ label: 'Large', value: 'large' }

									] }
								/>

							</PanelRow>

						}

						<PanelRow>

							<SelectControl
								label={ __( 'Container Size' ) }
								value={ attributes.groupSize }
								onChange={ ( value ) => setAttributes({ groupSize: value })}
								options={ [
									{ label: 'Standard', value: 'standard' },
									{ label: 'Small', value: 'small' }

								] }
							/>

						</PanelRow>

						<PanelRow>

							<CheckboxControl
								label="Add Introduction"
								checked={attributes.addIntro}
								onChange={(newval) => setAttributes({ addIntro: newval })}
							/>

						</PanelRow>


						<PanelRow>

							<CheckboxControl
								label="Remove Wrapper Padding"
								checked={attributes.removeWrapperPadding}
								onChange={(newval) => setAttributes({ removeWrapperPadding: newval })}
							/>

						</PanelRow>

					</PanelBody>

			</InspectorControls>
 
			 <div className={`wrapper ${ attributes.removeWrapperPadding ? 'p-0' : '' }`}>
 
				 <div className="container">

					<div class="row">

					    <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />

				    </div>

				 </div>
 
			 </div>
 
		 </>
	 );
 };
 export default Edit;
 