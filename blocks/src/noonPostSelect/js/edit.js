/**
 * WordPress dependencies
 */

 import { __ } from '@wordpress/i18n';
 import apiFetch from '@wordpress/api-fetch';
 import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
 import { PanelBody, PanelRow } from '@wordpress/components';
 const { PostRelationshipControl } = gumponents.components;


 // Carousel 

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';


 const Edit = ( props ) => {
	 const {
		 attributes: { pages },
		 setAttributes,
	 } = props;	
 
	 const blockProps = useBlockProps();
	 return (

		<>
			<InspectorControls>

				<PanelBody
					title="Pages to Select"
					initialOpen={true}
				>

					<PanelRow>

						<PostRelationshipControl
							label="Select pages"
							help="Select pages"
							postTypes="page"
							value={ pages.map( page => page.ID ) }
							onSelect={ value => setAttributes( { pages : value } ) }
							buttonLabel="Select Pages"
							max="6"
						/>

					</PanelRow>

				</PanelBody>

			</InspectorControls>	

			<div class="wrapper">

				<div class="container">

					<div class="row">

						<Swiper
						spaceBetween={50}
						slidesPerView={3}
						onSlideChange={() => console.log('slide change')}
						onSwiper={(swiper) => console.log(swiper)}
						>
					
							{pages.map((item, i) => 

								<SwiperSlide>
									
									<div class="">

										{ item?.image && 

											<img src={  item?.image } class="" />

										}

										<p>post_id = {item.ID }</p>

										<h3>{ item.post_title }</h3>
										<p>{ item.post_excerpt }</p>
									
									</div>
									
								</SwiperSlide>
						
							)}

						</Swiper>

					</div>

				</div>

			</div>


		</>

	 );
 };
 export default Edit;
 