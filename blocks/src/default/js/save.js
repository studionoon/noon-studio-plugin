/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const {
         attributes: {  mediaURL },
     } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
         <div { ...blockProps }>
 
 
             <div className="container-fluid position-relative">
 
                 <div className="row">
 
                     <div className="container">
 
                         <div className="row">
 
                             <div className="col-12 col-md-6 left-half">
 
                                 <InnerBlocks.Content />
 
                             </div>
 
                             
                             { mediaURL && (
 
                             <div className="col-12 col-md-6 right-half">
 
                                 <img
                                     className='background_images'
                                     src={ mediaURL }
                                     alt={ __( 'Recipe Image', 'gutenberg-examples' ) }
                                 />
 
                             </div>
 
                             ) }
 
                         </div>
 
                     </div>
 
                 </div>
 
             </div>
             
         </div>
 
     );
 };
 export default Save;