import './scss/editor.scss';

wp.blocks.registerBlockStyle(
    'core/group',
    [
        {
            name: 'container',
            label: 'Container',
            style_handle: 'container',
            default: true
        }
    ]
);


 /**
  * WordPress Dependencies
  */
 const { __ } = wp.i18n;
 const { addFilter } = wp.hooks;
 const { Fragment }	= wp.element;
 const { InspectorControls  }	= wp.blockEditor;
 const { createHigherOrderComponent } = wp.compose;
 import { TextControl, Panel, PanelBody, PanelRow  } from '@wordpress/components';

 //restrict to specific block names
 const allowedBlocks = [ 'core/group' ];
 
 /**
  * Add custom attribute for mobile visibility.
  *
  * @param {Object} settings Settings for the block.
  *
  * @return {Object} settings Modified settings.
  */
 function addAttributes( settings ) {
   
   //check if object exists for old Gutenberg version compatibility
   //add allowedBlocks restriction
   if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
   
     settings.attributes = Object.assign( settings.attributes, {
       groupTitle:{ 
         type: 'string'
       }
     });
     
   }
 
   return settings;
 }
 
 /**
  * Add mobile visibility controls on Advanced Block Panel.
  *
  * @param {function} BlockEdit Block edit component.
  *
  * @return {function} BlockEdit Modified block edit component.
  */
 const addGroupTitle = createHigherOrderComponent( ( BlockEdit ) => {
   return ( props ) => {
 
     const {
       name,
       attributes,
       setAttributes,
       isSelected,
     } = props;
 
     const {
        groupTitle,
     } = attributes;
   

     return (

       <Fragment>

        { isSelected && allowedBlocks.includes( name ) &&

          <InspectorControls>

            <PanelBody>

              <PanelRow>

                  <TextControl
                    label="Group Title"
                    value={ attributes.groupTitle }
                    onChange={ ( value ) => setAttributes( { groupTitle : value } ) }
                  />

              </PanelRow>

            </PanelBody>

          </InspectorControls>

        }
        <BlockEdit {...props} />

       </Fragment>
     );
   };
 } );


addFilter(
    'blocks.registerBlockType',
    'noon/group-addattributes',
    addAttributes
);
  
addFilter(
    'editor.BlockEdit',
    'noon/add-title',
    addGroupTitle,
    
);