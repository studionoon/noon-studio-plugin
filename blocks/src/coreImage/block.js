/**
 * External Dependencies
 */
 import classnames from 'classnames';

 /**
  * WordPress Dependencies
  */
 const { __ } = wp.i18n;
 const { addFilter } = wp.hooks;
 const { Fragment }	= wp.element;
 const { InspectorControls }	= wp.blockEditor;
 const { createHigherOrderComponent } = wp.compose;
 const { PanelBody, PanelRow, TextControl, SelectControl, CheckboxControl, PanelColorSettings  } = wp.components;

 //restrict to specific block names
 const allowedBlocks = [ 'core/image' ];
 
 /**
  * Add custom attribute for mobile visibility.
  *
  * @param {Object} settings Settings for the block.
  *
  * @return {Object} settings Modified settings.
  */
    function addAttributes( settings ) {
    
        //check if object exists for old Gutenberg version compatibility
        //add allowedBlocks restriction
        if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
        
            settings.attributes = Object.assign( settings.attributes, {

                useImageSize: {
                    type: "boolean",
                    default: false
                }
            });
            
        }
    
    return settings;

    }
 
 /**
  * Add mobile visibility controls on Advanced Block Panel.
  *
  * @param {function} BlockEdit Block edit component.
  *
  * @return {function} BlockEdit Modified block edit component.
  */
 const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
   return ( props ) => {
 
    const {
       name,
       attributes,
       setAttributes,
       isSelected,
       clientId,
    } = props;
 
    const {
        uniqueID,
    } = attributes;
   
        return (

        <Fragment>

            <BlockEdit {...props} />
                { isSelected && allowedBlocks.includes( name ) &&

                <>

                    <InspectorControls>

                        <PanelBody
                            title="Use Image size"
                            initialOpen={true}
                        >

                            <PanelRow>

                                <CheckboxControl
                                    label="Use image dimensions"
                                    checked={attributes.useImageSize}
                                    onChange={(newval) => setAttributes({ useImageSize: newval })}
                                />

                            </PanelRow>

                        </PanelBody>

                    </InspectorControls>

           </>
        }
 
       </Fragment>
     );

   };

 }, 'withAdvancedControls');
 
 /**
  * Add custom element class in save element.
  *
  * @param {Object} extraProps     Block element.
  * @param {Object} blockType      Blocks object.
  * @param {Object} attributes     Blocks attributes.
  *
  * @return {Object} extraProps Modified block element.
  */
 function applyExtraClass( extraProps, blockType, attributes ) {
 
    const { useImageSizeDimensions } = attributes;
    
    if ( typeof useImageSizeDimensions !== 'undefined' && !useImageSizeDimensions && allowedBlocks.includes( blockType.name ) ) {
        extraProps.className = classnames( extraProps.className, 'wp-block-image-dimension' );
    }
    
    return extraProps;
 }
 
 //add filters
 
 addFilter(
   'blocks.registerBlockType',
   'editorskit/custom-attributes',
   addAttributes
 );
 
 addFilter(
   'editor.BlockEdit',
   'editorskit/custom-advanced-control',
   withAdvancedControls
 );
 
 addFilter(
   'blocks.getSaveContent.extraProps',
   'editorskit/applyExtraClass',
   applyExtraClass
 );