/**
 * WordPress dependencies
 */

import { __ } from '@wordpress/i18n';
import { Fragment } from '@wordpress/element'
import { useBlockProps, InspectorControls, RichText } from '@wordpress/block-editor';

 const Edit = ( props ) => {

	const { 
		setAttributes, 
		attributes: {
			quote,
			quoteName,
			quoteCompany,
		} 
	} = props;

	const blockProps = useBlockProps();

	return (

		<Fragment>
			
			<div className='content'>
				

				<RichText
					{ ...blockProps }
					className="quote"
					tagName="p" // The tag here is the element output and editable in the admin
					value={ quote } // Any existing content, either from the database or an attribute default
					allowedFormats={ [ ] } // Allow the content to be made bold or italic, but do not allow other formatting options
					onChange={ ( value ) => setAttributes( { quote: value }) }
				
				/>

				<RichText
					{ ...blockProps }
					className="quoteName"
					tagName="p" // The tag here is the element output and editable in the admin
					value={ quoteName } // Any existing content, either from the database or an attribute default
					allowedFormats={ [ ] } // Allow the content to be made bold or italic, but do not allow other formatting options
					onChange={ ( value ) => setAttributes( { quoteName: value }) }
				
				/>
				<RichText
					{ ...blockProps }
					className="quoteCompany"
					tagName="p" // The tag here is the element output and editable in the admin
					value={ quoteCompany } // Any existing content, either from the database or an attribute default
					allowedFormats={ [ ] } // Allow the content to be made bold or italic, but do not allow other formatting options
					onChange={ ( value ) => setAttributes( { quoteCompany: value }) }
				
				/>


			</div>
			
		</Fragment>

	);
 };
 export default Edit;
 