/**
 * WordPress dependencies
 */

import { __ } from '@wordpress/i18n';
import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';

const Edit = ( props ) => {
	const {
		 attributes,
		 setAttributes,
	} = props;	
 
	const blockProps = useBlockProps();

	const ALLOWED_BLOCKS = [ 'core/paragraph', 'core/list', 'core/heading', 'noon/carousel-image' ]

 
	return (

	<div className="container-fluid position-relative child-carousel-in-editor">

		<InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } />

	</div>


	);
};
export default Edit;
 