/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript.
 * You can create a new block folder in
 * this dir and include code for that block
 * here as well.
 *
 * All blocks should be included here since
 * this is the file that Webpack is compiling.
 */

//import './coreGroup/block.js';
import './coreList/block.js';
import './coreImage/block.js';
import './coreSpacer/block.js';
import './noonColumnHeader/block.js';
import './coreMediaText/block.js';
import './noonHeaderImage/block.js';


// Accordian
import './noonAccordianParent/block.js';
import './noonAccordianChild/block.js';
import './noonCoverImage/block.js';
import './noonGroup/block.js';

// Carousel
import './noonCarouselParent/block.js';
import './noonCarouselChild/block.js';
import './noonCarouselImage/block.js';


import './noonPostSelect/block.js';
import './noonPartners/block.js';

import './noonPostSelect/block.js';

import './noonQuote/block';
