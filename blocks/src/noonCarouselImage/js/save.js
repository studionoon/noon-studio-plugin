/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const {
         attributes: {  mediaURL },
     } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
         <div { ...blockProps }>

            { mediaURL && (

                <div className="col-12 col-md-6 right-half">

                    <img
                        className='background_images'
                        src={ mediaURL }
                        alt={ __( 'Recipe Image', 'gutenberg-examples' ) }
                    />

                </div>  

                )

            }
                             
         </div>
 
     );
 };
 export default Save;