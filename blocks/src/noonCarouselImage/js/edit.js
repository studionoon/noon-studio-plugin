/**
 * WordPress dependencies
 * 
 */

 import { __ } from '@wordpress/i18n';
 import { MediaUpload, useBlockProps, InspectorControls } from '@wordpress/block-editor';
 import { Button, FocalPointPicker, PanelBody } from '@wordpress/components';
 
 
 const Edit = ( props ) => {
	 const {
		 attributes: { mediaID, mediaURL, focalPoint  },
		 setAttributes,
	 } = props;	
 
	 const blockProps = useBlockProps();
 
	 const onSelectImage = ( media ) => {
		 setAttributes( {
			 mediaURL: media.url,
			 mediaID: media.id,
		 } );
	 };
 
	 return (

		 <>
			 

        <InspectorControls>

            <PanelBody title={ __( 'Media settings' ) }>
                
                <FocalPointPicker
                    label={ __( 'Focal point picker' ) }
                    url={ mediaURL }
                    value={ focalPoint }
                    onChange={ ( newFocalPoint ) =>
                        setAttributes( {
                            focalPoint: newFocalPoint,
                        } )
                    }
                />

			<MediaUpload
				onSelect={ onSelectImage }
				allowedTypes="image"
				value={ mediaID }
				render={ ( { open } ) => (

				<Button
					className={ 'button button-large'}
					onClick={ open }
					>
					{ ! mediaID ? (

						__('Upload Image')

					) : ( 

						__('Change Image')

					) }
				
				
				</Button>

				)}

			/>

                
            </PanelBody>

        </InspectorControls>
 
		<div class="image_wapper">

			{ ! mediaID ? (


				<Button
					className={
						mediaID ? 'image-button' : 'button button-large'
					}
					onClick={ open }
					>
					__( 'Upload Image', 'gutenberg-examples' )
				
				</Button>


			) : (

				<img
				className='background_images'
				src={ mediaURL }
				/>

			)}

		 </div>

		</>

	 );
 };
 export default Edit;
 