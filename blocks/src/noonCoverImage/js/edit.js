/**
 * WordPress dependencies
 */

 import { __ } from '@wordpress/i18n';
 import { MediaUpload, InspectorControls,  useBlockProps } from '@wordpress/block-editor';
 import { Button, FocalPointPicker, PanelBody } from '@wordpress/components';
 
 const Edit = ( props ) => {
	 const {
		 attributes: { mediaID, mediaURL, focalPoint },
		 setAttributes,
	 } = props;	
 
	 const blockProps = useBlockProps();
 
	 const onSelectImage = ( media ) => {
		 setAttributes( {
			 mediaURL: media.url,
			 mediaID: media.id,
		 } );
	 };
 

	 const objectPosition = focalPoint ? `${ Math.round( focalPoint.x * 100 ) }% ${ Math.round( focalPoint.y * 100 ) }%` : undefined;

	 // Focal Preview

	 return (
		 <>

			<InspectorControls>

				<PanelBody title={ __( 'Media settings' ) }>

					{ mediaID &&  

						<FocalPointPicker
							label={ __( 'Focal point picker' ) }
							url={ mediaURL }
							value={ focalPoint }
							onChange={ ( newFocalPoint ) =>
								setAttributes( {
									focalPoint: newFocalPoint,
								} )
							}
						/>

					}

				<MediaUpload
					onSelect={ onSelectImage }
					allowedTypes="image"
					value={ mediaID }
					render={ ( { open } ) => (
						<Button
							className={ 'button button-large' }
							onClick={ open }
						>
							{ ! mediaID ? (
								__( 'Upload Image', 'gutenberg-examples' )
							) : (
								__( 'Change Image', 'gutenberg-examples' )
							) }
						</Button>
					) }
				/>

					
				</PanelBody>	

			</InspectorControls>

			<div class="wrapper">

				{ mediaID && 
	
					<img
						style={ { 'object-position' : objectPosition } }
						className='background_images'
						src={ mediaURL }
					/>	

				}

			</div>

		</>
	 );
 };
 export default Edit;
 