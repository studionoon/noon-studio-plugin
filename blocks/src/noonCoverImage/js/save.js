/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const {
         attributes: {  mediaURL },
     } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
         <div { ...blockProps }>

            <img
                className='background_images'
                src={ mediaURL }
                alt={ __( 'Cover Image', 'gutenberg-examples' ) }
            />
                             
         </div>
 
     );
 };
 export default Save;