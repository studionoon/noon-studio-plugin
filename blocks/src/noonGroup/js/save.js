/**
 * WordPress dependencies
 */
 import { __ } from '@wordpress/i18n';
 import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';
 
 const Save = ( props ) => {
 
     const {
         attributes: {  groupTitle, groupIntro },
     } = props;
 
     const blockProps = useBlockProps.save();
 
     return (
         <div { ...blockProps }>
 
 
             <div className="wrapper">
 
                 <div className="container">
 
                    <InnerBlocks.Content />

                </div>

             </div>
             
         </div>
 
     );
 };
 export default Save;