/**
 * External Dependencies
 */
 import classnames from 'classnames';

 /**
  * WordPress Dependencies
  */
 const { __ } = wp.i18n;
 const { addFilter } = wp.hooks;
 const { Fragment }	= wp.element;
 const { InspectorControls }	= wp.blockEditor;
 const { createHigherOrderComponent } = wp.compose;
 const { PanelBody, PanelRow, TextControl, SelectControl, CheckboxControl, PanelColorSettings  } = wp.components;

 //restrict to specific block names
 const allowedBlocks = [ 'core/media-text', 'noon/columnheader' ];
 
 /**
  * Add custom attribute for mobile visibility.
  *
  * @param {Object} settings Settings for the block.
  *
  * @return {Object} settings Modified settings.
  */
    function addAttributes( settings ) {
    
    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
    
        settings.attributes = Object.assign( settings.attributes, {

            uniqueID: {
                type: "string"
            },
    
            imageBreakpoints:{
                type : "object"
            },

            image_height_xs:{
                type : "string"
            },
            image_height_sm:{
                type : "string"
            },
            image_height_md:{
                type : "string"
            },
            image_height_lg:{
                type : "string"
            },
            image_height_xl:{
                type : "string"
            },
            image_height_xxl:{
                type : "string"
            },

            imageBreakpointUnits:{
                type : "object"
            },

            image_unit_xs:{
                type : "string"
            },
            image_unit_sm:{
                type : "string"
            },
            image_unit_md:{
                type : "string"
            },
            image_unit_lg:{
                type : "string"
            },
            image_unit_xl:{
                type : "string"
            },
            image_unit_xxl:{
                type : "string"
            },
            addBg: {
                type: "boolean",
                default: false
            },
            halfPage: {
                type: "boolean",
                default: false
            },
            customBgColor: {
                type: 'string'
            }
        });

        
        }
    
    return settings;

    }
    
 
 /**
  * Add mobile visibility controls on Advanced Block Panel.
  *
  * @param {function} BlockEdit Block edit component.
  *
  * @return {function} BlockEdit Modified block edit component.
  */
 const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
   return ( props ) => {
 
    const {
       name,
       attributes,
       setAttributes,
       isSelected,
       clientId,
    } = props;
 
    const {
        image_height_xs,
        image_unit_xs,
        uniqueID,
    } = attributes;


    if ( ! uniqueID ) {
        setAttributes( { uniqueID: clientId } );
    }

   
    let imageBreakpoint = attributes.imageBreakpoints;
    let imageBreakpointUnit = attributes.imageBreakpointUnits;
    
        // Breakpoints
        function onImageBreakpointChange( value, size ) {
    
            if( typeof imageBreakpoint === 'object' && imageBreakpoint !== null ){
    
                Object.assign(imageBreakpoint, { [size] : value } );
    
            } else {
    
                imageBreakpoint = new Object( { [size] : value }  )
            }
    
            // works very much like setState
            setAttributes( { imageBreakpoints: imageBreakpoint } );
    
        }
    
    
        // Breakpoints
        function onImageBreakpointUnitChange( value, size ) {
    
            if( typeof imageBreakpointUnit === 'object' && imageBreakpointUnit !== null ){
    
                Object.assign(imageBreakpointUnit, { [size] : value} );
    
            } else {
    
                imageBreakpointUnit = new Object( { [size] : value } )
            }
    
            // works very much like setState
            setAttributes( { imageBreakpointUnits: imageBreakpointUnit } );
        
        }

        return (

        <Fragment>

            <BlockEdit {...props} />
                { isSelected && allowedBlocks.includes( name ) &&

                <>

            <InspectorControls>

                <PanelBody
                    title="Slide Heights"
                    initialOpen={false}
                    className="image_height"
                >

                    <PanelRow>

                        <TextControl 
                            label="Image height at XS"
                            value={ attributes.image_height_xs }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'XS' ); setAttributes( { image_height_xs: value } ); } }
                            type="number"
                        />

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_xs }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XS' ); setAttributes( { image_unit_xs: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                    <PanelRow>

                        <TextControl 
                            label="Image height at SM"
                            value={ attributes.image_height_sm }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'SM' ); setAttributes( { image_height_sm: value } ); } }
                            type="number"
                        />

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_sm }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'SM' ); setAttributes( { image_unit_sm: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                    <PanelRow>

                        <TextControl 
                            label="Image height at MD"
                            value={ attributes.image_height_md }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'MD' ); setAttributes( { image_height_md: value } ); } }
                            type="number"
                        />

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_md }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'MD' ); setAttributes( { image_unit_md: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                    <PanelRow>

                        <TextControl 
                            label="Image height at LG"
                            value={ attributes.image_height_lg }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'LG' ); setAttributes( { image_height_lg: value } ); } }
                            type="number"
                        /> 

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_lg }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'LG' ); setAttributes( { image_unit_lg: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                    <PanelRow>

                        <TextControl 
                            label="Image height at XL"
                            value={ attributes.image_height_xl }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'XL' ); setAttributes( { image_height_xl: value } ); } }
                            type="number"
                        />

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_xl }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XL' ); setAttributes( { image_unit_xl: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                    <PanelRow>

                        <TextControl 
                            label="Image height at XXL"
                            value={ attributes.image_height_xxl }
                            onChange={ ( value ) => { onImageBreakpointChange( value, 'XXL' ); setAttributes( { image_height_xxl: value } ); } }
                            type="number"
                        />

                        <SelectControl
                            label={ __( 'Units' ) }
                            value={ attributes.image_unit_xxl }
                            onChange={ ( value ) => { onImageBreakpointUnitChange( value, 'XXL' ); setAttributes( { image_unit_xxl: value } ); } }
                            options={ [
                                { label: 'px', value: 'px' },
                                { label: 'vh', value: 'vh' },
                                { label: 'vw', value: 'vw' },
                                { label: 'rem', value: 'rem' },
                            ] }
                        />

                    </PanelRow>

                </PanelBody>

                <PanelBody
					title="Group Options"
					initialOpen={true}
				>

					<PanelRow>

						<CheckboxControl
							label="Add Background"
							checked={attributes.addBg}
							onChange={( value ) => setAttributes({ addBg: value })}
						/>

					</PanelRow>

					<PanelRow>

						<CheckboxControl
							label="Half Width Image"
							checked={attributes.halfPage}
							onChange={( value ) => setAttributes({ halfPage: value })}
						/>

					</PanelRow>

				</PanelBody>


           </InspectorControls>

           </>
        }
 
       </Fragment>
     );

   };

 }, 'withAdvancedControls');
 
 /**
  * Add custom element class in save element.
  *
  * @param {Object} extraProps     Block element.
  * @param {Object} blockType      Blocks object.
  * @param {Object} attributes     Blocks attributes.
  *
  * @return {Object} extraProps Modified block element.
  */
 function applyExtraClass( extraProps, blockType, attributes ) {
 
   const { addListCheckMark } = attributes;
   
   if ( typeof addListCheckMark !== 'undefined' && !addListCheckMark && allowedBlocks.includes( blockType.name ) ) {
     extraProps.className = classnames( extraProps.className, 'noon-rotate-img' );
   }
 
   return extraProps;
 }
 
 //add filters
 
 addFilter(
   'blocks.registerBlockType',
   'editorskit/custom-attributes',
   addAttributes
 );
 
 addFilter(
   'editor.BlockEdit',
   'editorskit/custom-advanced-control',
   withAdvancedControls
 );
 
 addFilter(
   'blocks.getSaveContent.extraProps',
   'editorskit/applyExtraClass',
   applyExtraClass
 );